#! /usr/bin/env python

import argparse
from collections import defaultdict as DefaultDict
import shlex
import subprocess

def execute(command):
    try:
        process = subprocess.Popen(
            shlex.split(command),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
    except OSError:
        details = (None, None)
    else:
        details = process.communicate()

    return [(x if x else "") for x in details]

def parse_interface_and_macs():
    output, error = execute("ifconfig -a")
    interfaces, macs = dict(), DefaultDict(list)

    for line in output.splitlines():
        if not line.split() or line.startswith(" "):
            continue
        line = line.split()
        
        interface = line[0]
        mac = line[-1]
        
        interfaces[interface] = mac
        macs[mac] += interface
        
    return interfaces, macs

def parse_connection_type(interfaces, macs):
    output, error = execute("iwconfig")
    wired, wireless = dict(), dict()

    wired = parse_iwconfig(wired, macs, error)
    wireless = parse_iwconfig(wireless, macs, output)

    return wired, wireless

def parse_iwconfig(interfaces, macs, details):
    for line in details.splitlines():
        if not line.split() or line.startswith(" "):
            continue
        line = line.split()

        interface = line[0]
        interfaces[interface] = macs[interface]

    return interfaces


if __name__ == "__main__":
    interfaces, macs = parse_interface_and_macs()
    wired, wireless = parse_connection_type(interfaces, macs)

    display_items = lambda items, type_: [
        ",".join([iface, type_, interfaces[iface]]) for iface in items]
    
    print("\n".join(display_items(wired, "wired")))
    print("\n".join(display_items(wireless, "wireless")))
