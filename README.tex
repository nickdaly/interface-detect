% Created 2018-02-06 Tue 08:55
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fixltx2e}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{color}
\usepackage{listings}
\usepackage{framed}
\usepackage[letterpaper,margin=0.9in]{geometry}
\author{Nick Daly}
\date{2018-02-06 Tue}
\title{Interface Detection}
\hypersetup{
 pdfauthor={Nick Daly},
 pdftitle={Interface Detection},
 pdfkeywords={},
 pdfsubject={Displays information about network interfaces.},
 pdfcreator={Emacs 24.5.1 (Org mode 8.3.3)},
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents


\section{Overview}
\label{sec:orgheadline4}

This script displays connection information about the unique network interfaces it detects.  It displays output in the form:

\begin{verbatim}
interface-name,(wired|wireless),MAC
\end{verbatim}

This script has three basic parts:

\begin{description}
\item[{\hyperref[sec:orgheadline1]{Interface detection logic}}] Parse output from \emph{ifconfig} and \emph{iwconfig}.
\item[{\hyperref[sec:orgheadline2]{Main program flow}}] Sort and display program output.
\item[{\hyperref[sec:orgheadline3]{Utility functions}}] Imports and other sundry operational abstractions.
\end{description}

\emph{\texttt{<<interface-detect.py>>}} =
\begin{framed}
\lstset{language=Python,label= ,caption= ,captionpos=b,numbers=left}
\begin{lstlisting}
#! /usr/bin/env python

<<utilities>>
<<detect-interfaces>>

<<main>>
\end{lstlisting}
\end{framed}


\section{Interface Detection Logic}
\label{sec:orgheadline1}

The interface detection logic has two parts:

\begin{enumerate}
\item Associating all interfaces with their MAC addresses.
\item Identifying which interfaces are wired and which are wireless.
\end{enumerate}

\emph{\texttt{<<detect-interfaces>>}} =
\begin{framed}
\lstset{language=Python,label= ,caption= ,captionpos=b,numbers=left}
\begin{lstlisting}
def parse_interface_and_macs():
    output, error = execute("ifconfig -a")
    interfaces, macs = dict(), DefaultDict(list)

    for line in output.splitlines():
	<<identify-ifconfig-interfaces>>
    return interfaces, macs

def parse_connection_type(interfaces, macs):
    output, error = execute("iwconfig")
    wired, wireless = dict(), dict()

    wired = parse_iwconfig(wired, macs, error)
    wireless = parse_iwconfig(wireless, macs, output)

    return wired, wireless

<<identify-connection-type>>
\end{lstlisting}
\end{framed}


\subsection{Identify Interfaces from \emph{ifconfig}}
\label{sec:orgheadline5}

Parse the interface data from the \emph{ifconfig} output.  Each \emph{ifconfig} line that identifies an interface starts with the interface's name and ends with the interface's MAC address.  Record both of those items.

Since \emph{ifconfig} can display interfaces that have aliases, multiple interfaces can refer to a single MAC address.  This is annoying and could throw off our counts of real, physical, interfaces.  So, the MAC address list needs to be able to store multiple interfaces for each MAC address, since we don't know if the real interface name or the alias occurs first.

\emph{\texttt{<<identify-ifconfig-interfaces>>}} =
\begin{framed}
\lstset{language=Python,label= ,caption= ,captionpos=b,numbers=left}
\begin{lstlisting}
<<filter-useless-lines>>

interface = line[0]
mac = line[-1]

interfaces[interface] = mac
macs[mac] += interface
\end{lstlisting}
\end{framed}


Filter out empty lines and lines that start with white-space.

\emph{\texttt{<<filter-useless-lines>>}} =
\begin{framed}
\lstset{language=Python,label= ,caption= ,captionpos=b,numbers=left}
\begin{lstlisting}
if not line.split() or line.startswith(" "):
    continue
line = line.split()
\end{lstlisting}
\end{framed}


\subsection{Identify Connection Type from \emph{iwconfig}}
\label{sec:orgheadline6}

Parse the interface data from the \emph{iwconfig} output.  Each \emph{iwconfig} line that identifies an interface starts with the interface's name and contains data about the networks supported or the line \texttt{no wireless extensions.}, if the interface is a wired interface.  Record the interface's name and whether it's a wireless interface or not.

\emph{iwconfig} doesn't currently appear to display interface aliases, so we can use its output to filter out the aliases that don't refer to real, physical interfaces.

\emph{\texttt{<<identify-connection-type>>}} =
\begin{framed}
\lstset{language=Python,label= ,caption= ,captionpos=b,numbers=left}
\begin{lstlisting}
def parse_iwconfig(interfaces, macs, details):
    for line in details.splitlines():
	<<filter-useless-lines>>

	interface = line[0]
	interfaces[interface] = macs[interface]

    return interfaces
\end{lstlisting}
\end{framed}


\section{Main Program Flow}
\label{sec:orgheadline2}

The main program flow has two parts:

\begin{enumerate}
\item Identify interface details.
\item Display the results.
\end{enumerate}

\emph{\texttt{<<main>>}} =
\begin{framed}
\lstset{language=Python,label= ,caption= ,captionpos=b,numbers=left}
\begin{lstlisting}
if __name__ == "__main__":
    <<process-interfaces>>

    <<display-results>>
\end{lstlisting}
\end{framed}


\subsection{Interface Processing}
\label{sec:orgheadline7}

Interpret arguments and print the results of the interface search.

\emph{\texttt{<<process-interfaces>>}} =
\begin{framed}
\lstset{language=Python,label= ,caption= ,captionpos=b,numbers=left}
\begin{lstlisting}
interfaces, macs = parse_interface_and_macs()
wired, wireless = parse_connection_type(interfaces, macs)
\end{lstlisting}
\end{framed}


These functions are detailed in \hyperref[sec:orgheadline1]{Interface Detection Logic}.

\subsection{Display Results}
\label{sec:orgheadline8}

Print the interface name, connection type, and MAC address of each interface.

\emph{\texttt{<<display-results>>}} =
\begin{framed}
\lstset{language=Python,label= ,caption= ,captionpos=b,numbers=left}
\begin{lstlisting}
display_items = lambda items, type_: [
    ",".join([iface, type_, interfaces[iface]]) for iface in items]

print("\n".join(display_items(wired, "wired")))
print("\n".join(display_items(wireless, "wireless")))
\end{lstlisting}
\end{framed}


\section{Utility Functions}
\label{sec:orgheadline3}

Import necessary libraries and create a function to return a command's output:

Execute will return empty strings instead of \emph{None} if the specified command had no output or if it errored.  Those empty strings are ignored during later processing.

\emph{\texttt{<<utilities>>}} =
\begin{framed}
\lstset{language=Python,label= ,caption= ,captionpos=b,numbers=left}
\begin{lstlisting}
import argparse
from collections import defaultdict as DefaultDict
import shlex
import subprocess

def execute(command):
    try:
	process = subprocess.Popen(
	    shlex.split(command),
	    stdout=subprocess.PIPE,
	    stderr=subprocess.PIPE)
    except OSError:
	details = (None, None)
    else:
	details = process.communicate()

    return [(x if x else "") for x in details]
\end{lstlisting}
\end{framed}


\newpage

\section{\texttt{interface\_detect.py} Listing}
\label{sec:orgheadline9}

The complete source code for this program follows.

\emph{\texttt{<<interface\_detect.py>>}} =
\begin{framed}
\lstset{language=Python,label= ,caption= ,captionpos=b,numbers=left}
\begin{lstlisting}
#! /usr/bin/env python

import argparse
from collections import defaultdict as DefaultDict
import shlex
import subprocess

def execute(command):
    try:
	process = subprocess.Popen(
	    shlex.split(command),
	    stdout=subprocess.PIPE,
	    stderr=subprocess.PIPE)
    except OSError:
	details = (None, None)
    else:
	details = process.communicate()

    return [(x if x else "") for x in details]

def parse_interface_and_macs():
    output, error = execute("ifconfig -a")
    interfaces, macs = dict(), DefaultDict(list)

    for line in output.splitlines():
	if not line.split() or line.startswith(" "):
	    continue
	line = line.split()

	interface = line[0]
	mac = line[-1]

	interfaces[interface] = mac
	macs[mac] += interface

    return interfaces, macs

def parse_connection_type(interfaces, macs):
    output, error = execute("iwconfig")
    wired, wireless = dict(), dict()

    wired = parse_iwconfig(wired, macs, error)
    wireless = parse_iwconfig(wireless, macs, output)

    return wired, wireless

def parse_iwconfig(interfaces, macs, details):
    for line in details.splitlines():
	if not line.split() or line.startswith(" "):
	    continue
	line = line.split()

	interface = line[0]
	interfaces[interface] = macs[interface]

    return interfaces


if __name__ == "__main__":
    interfaces, macs = parse_interface_and_macs()
    wired, wireless = parse_connection_type(interfaces, macs)

    display_items = lambda items, type_: [
	",".join([iface, type_, interfaces[iface]]) for iface in items]

    print("\n".join(display_items(wired, "wired")))
    print("\n".join(display_items(wireless, "wireless")))
\end{lstlisting}
\end{framed}


\section{Meta}
\label{sec:orgheadline14}

This document was created with \href{https://orgmode.org}{Org Mode}.  Several formats, including executable code, can be created from the source files.  The source is available:

\begin{itemize}
\item \url{https://github.com/NickDaly/interface-detect}
\end{itemize}

\subsection{Revisions}
\label{sec:orgheadline10}

\begin{description}
\item[{\textit{[2013-12-28 Sat]}}] First published.
\item[{\textit{[2018-02-06 Tue]}}] Significant formatting improvements to PDF export.
\end{description}

\subsection{Copying: GPLv3+}
\label{sec:orgheadline11}

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see \url{http://www.gnu.org/licenses/}.

\subsection{Tangle Before Export}
\label{sec:orgheadline12}

To correctly include \texttt{interface\_detect.py} when exporting to PDF, make sure to tangle before exporting.

\emph{\texttt{<<tangle-before-export>>}} =
\begin{framed}
\lstset{language=elisp,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
(progn
  (org-babel-tangle)
  (org-latex-export-to-pdf))
\end{lstlisting}
\end{framed}


\subsection{Local Variables for Source Block Naming and \LaTeX{} Framing}
\label{sec:orgheadline13}

These local variables make sure each source block is nicely formatted.
\end{document}
